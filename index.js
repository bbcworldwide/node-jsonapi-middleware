const HttpStatus = require('http-status-codes')
const bodyParser = require('body-parser')

const defaultOptions = {
  validateContentType: true,
  validateAccepts: true,
  bodyLimit: '50mb',
  meta: null
}

const TYPE = 'application/vnd.api+json'

module.exports = (userOptions = {}) => {
  const options = Object.assign({}, defaultOptions, userOptions)

  const jsonParser = bodyParser.json({
    type: 'application/vnd.api+json',
    limit: options.bodyLimit
  })

  return function (req, res, next) {
    res.set('Content-Type', TYPE)

    const fields = req.query.fields ? parseFields(req.query.fields) : {}
    req.jsonApi = {
      fields
    }

    // Extend response object to add jsonApi responder
    // This needs to be added at runtime rather than on the object's prototype as it can contain metadata from the request.
    res.jsonApi = (payload) => {
      if (options.meta) {
        // Meta can be object or callback
        if (typeof options.meta === 'function') {
          payload.meta = options.meta(req, res, payload)
        } else {
          payload.meta = options.meta
        }
      }
      if (fields && payload.data) {
        payload.data = filterFields(payload.data, fields)
      }
      res.send(new Buffer(JSON.stringify(payload)))
    }

    const accept = req.header('Accept')
    const contentType = req.header('Content-Type')
    const method = req.method

    // Enforce header rules from JSON:API spec:
    if (options.validateContentType) {
      // Require correct content type for methods that send data:
      const hasBody = (method === 'POST' || method === 'PATCH' || method === 'PUT')
      if (hasBody && contentType !== TYPE) {
        console.log(contentType)
        return res.sendStatus(HttpStatus.UNSUPPORTED_MEDIA_TYPE)
      }
    }

    if (options.validateAccepts) {
      // If an accepts header is sent it must contain the JSON:API content type with no modifiers.
      if (accept && accept.indexOf(TYPE) !== -1 && accept.split(', ').indexOf(TYPE) === -1) {
        return res.sendStatus(HttpStatus.NOT_ACCEPTABLE)
      }
    }

    // Forward on to body parser
    jsonParser(req, res, next)
  }
}

// Parse 'fields' query parameter
function parseFields (fieldsQuery) {
  const parsed = {}
  Object.keys(fieldsQuery).forEach((type) => {
    parsed[type] = fieldsQuery[type].split(',')
  })
  return parsed
}

// Transform resonse to apply sparse fieldsets

function filterFields (data, fields) {
  return Array.isArray(data)
    ? data.map(resource => filterResource(resource, fields))
    : filterResource(data, fields)
}

function filterResource (resource, fields) {
  if (resource && resource.id && resource.type && fields[resource.type]) {
    const typeFields = fields[resource.type]
    const filtered = Object.assign({}, resource)
    if (resource.attributes) {
      filtered.attributes = filterProperties(resource.attributes, typeFields)
    }
    if (resource.relationships) {
      filtered.relationships = filterProperties(resource.relationships, typeFields)
    }
    return filtered
  }
  return resource
}

function filterProperties (obj, whitelist) {
  const filtered = {}
  Object.keys(obj).forEach((key) => {
    if (whitelist.indexOf(key) !== -1) {
      filtered[key] = obj[key]
    }
  })
  return filtered
}

